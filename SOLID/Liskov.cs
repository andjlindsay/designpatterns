﻿using System;

namespace SOLID
{
    class Rectangle
    {
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        public Rectangle()
        {
        }

        public Rectangle(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public override string ToString()
        {
            return $"{nameof(Width)}: {Width}, {nameof(Height)}: {Height}";
        }
    }

    class Square : Rectangle
    {
        public override int Height
        {
            set { base.Height = base.Width = value; }
        }
        public override int Width
        {
            set { base.Height = base.Width = value; }
        }
        public Square()
        {
        }
    }

    class Liskov
    {
        public static string Area(Rectangle rc) => $"Area {rc.Width * rc.Height}";
        public static void Demo()
        {
            Rectangle rec = new Square();
            rec.Width = 2;
            Console.WriteLine($"{rec}, {Area(rec)}");
        }
    }
}
