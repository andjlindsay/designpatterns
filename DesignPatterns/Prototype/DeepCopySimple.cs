﻿using System;

namespace DesignPatterns.Prototype.Simple
{
    public class Point
    {
        public int X, Y;

        public Point()
        {
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(Point toCopy)
        {
            X = toCopy.X;
            Y = toCopy.Y;
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }
    }

    public class Line
    {
        public Point Start, End;

        public Line(Point start, Point end)
        {
            Start = start;
            End = end;
        }

        public Line()
        {
        }

        public Line DeepCopy()
        {
            return new Line(
                new Point(this.Start)
                , new Point(this.End)
            );
        }

        public override string ToString()
        {
            return $"Start: ({Start}), ({End})";
        }
    }

    public static class Demo
    {
        public static void Run(string[] args)
        {
            var line1 = new Line { Start = new Point(1, 2), End = new Point(1, 5) };
            var line2 = line1.DeepCopy();
            line2.Start = new Point(8, 5);
            line2.End = new Point(12, 5);

            Console.WriteLine(line1);
            Console.WriteLine(line2);
        }
    }
}
