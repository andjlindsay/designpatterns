﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DesignPatterns.Prototype
{
    public static class Extension
    {
        public static T DeepCopy<T>(this T self)
        {
            using (var stream = new MemoryStream()) {
                BinaryFormatter s = new BinaryFormatter();
                s.Serialize(stream, self);
                stream.Position = 0;
                return (T)s.Deserialize(stream);
            }
        }
    }

    [Serializable]
    public class Point
    {
        public int X, Y;

        public Point()
        {
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }
    }

    [Serializable]
    public class Line
    {
        public Point Start, End;

        public Line()
        {
        }

        public Line DeepCopy()
        {
            return Extension.DeepCopy(this);
        }

        public override string ToString()
        {
            return $"Start: ({Start}), ({End})";
        }
    }

    public static class Demo
    {
        public static void Run(string[] args)
        {
            var line1 = new Line { Start = new Point(1, 2), End = new Point(1, 5) };
            var line2 = line1.DeepCopy();
            line2.Start = new Point(8, 5);
            line2.End = new Point(12, 5);

            Console.WriteLine(line1);
            Console.WriteLine(line2);
        }
    }
}
