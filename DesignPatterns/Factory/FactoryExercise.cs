﻿using System;

namespace DesignPatterns.FactoryMethod
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Person(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return $"{Id}: {Name}";
        }
    }

    public class PersonFactory
    {
        private int currentId = 0;

        public PersonFactory(){}

        public Person CreatePerson(string Name)
        {
            return new Person(currentId++, Name);
        }
        public class Demo
        {
            public static void Run()
            {
                var pf = new PersonFactory();
                Console.WriteLine(pf.CreatePerson("Steve"));
                Console.WriteLine(pf.CreatePerson("Jimmothy"));
                Console.WriteLine(pf.CreatePerson("Yolanda"));
            }
        }
    }

}