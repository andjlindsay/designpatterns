﻿using System;

namespace DesignPatterns.Singleton
{
    
    public class SingletonTester
    {
        public static bool IsSingleton(Func<object> func)
        {
            var o1 = func();
            var o2 = func();
            return ReferenceEquals(o1,o2);
        }
    }

    public static class Demo
    {
        public static void Run(string[] args)
        {
            var obj = new object();
            Console.WriteLine(SingletonTester.IsSingleton(() => obj));
            Console.WriteLine(SingletonTester.IsSingleton(() => new Object()));
        }
    }
}

