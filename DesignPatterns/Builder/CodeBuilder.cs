﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.CodeBuilder
{
    public class CodeBuilder
    {
        private readonly string className;
        private List<Tuple<string,string>> properties = new List<Tuple<string, string>>();

        public CodeBuilder(string className)
        {
            this.className = className;
        }

        public CodeBuilder AddField(string propertyName, string dataType)
        {
            properties.Add(new Tuple<string, string>(propertyName, dataType));
            return this;
        }

        public override string ToString()
        {
            string retVal = $"public class {className}"+"\n{\n";
            properties.ForEach(p => retVal += $"  public {p.Item2} {p.Item1};" + "\n");
            return retVal + "}";
        }
    }

    public class Demo
    {
        public static void Run()
        {
            var cb = new CodeBuilder("Person")
                .AddField("Name", "string")
                .AddField("Age", "int");
            Console.WriteLine(cb);
        }
    }
}