﻿using System;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Builder.Demo.Run();
            CodeBuilder.Demo.Run();
            FactoryMethod.PersonFactory.Demo.Run();
            Prototype.Demo.Run(args);
            Prototype.Simple.Demo.Run(args);
            */
            Coding.Exercise.Demo.Run(args);
        }
    }
}