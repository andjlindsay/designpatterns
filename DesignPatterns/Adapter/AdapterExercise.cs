﻿using System;

namespace Coding.Exercise
{
    public class Square
    {
        public int Side { get; set; }
    }

    public interface IRectangle
    {
        int Width { get; set; }
        int Height { get; set; }
    }

    public static class ExtensionMethods
    {
        public static int Area(this IRectangle rc)
        {
            return rc.Width * rc.Height;
        }
    }

    public class SquareToRectangleAdapter : IRectangle
    {
        public SquareToRectangleAdapter(Square square)
        {
            Width = Height = square.Side;
        }

        public int Width { get; set; }
        public int Height { get; set; }

        public override string ToString()
        {
            return $"Width: {Width}, Height: {Height}, Area: {ExtensionMethods.Area(this)}";
        }
    }

    public static class Demo
    {
        public static void Run(string[] args)
        {
            var sq = new Square { Side = 11 };
            Console.WriteLine(new SquareToRectangleAdapter(sq));
        }
    }
}
